<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forecast extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Develope by Rajan Srivastava rajancs007@gmail.com
	 */

	public function index()
	{
	$this->load->database();
		$this->load->model('weather');
		$data =	$this->weather->connect();	
			$data = json_decode(json_encode($data), true);
		$this->load->view('main',$data);
		
		 if($this->input->post('save'))
		{
		   
			  $data = array(  
                        'name'     => $this->input->post('name'),  
                        'description'  => $this->input->post('description'),  
                        'temp_max'   => $this->input->post('temp_max'),  
                        'temp_min' => $this->input->post('temp_min'),
						'humidity' => $this->input->post('humidity') ,
						'speed' => $this->input->post('speed') 						
                        );  
			//$user=$this->weather->save_whetherDetails($data);
			 $user= $this->db->insert('weather_info',$data);  
			if($user>0){
			        echo "Records Saved Successfully";
			}
			else{
					echo "Insert error !";
			}
		}
	}


}
