<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Palindrome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Develope by Rajan Srivastava rajancs007@gmail.com
	 */

	public function index()	{
		$input = 1235321;  
		$num = $this->PalindromeNumber($input);  
		if($input==$num){  
		echo "$input is a Palindrome number";  
		} else {  
		echo "$input is not a Palindrome";  
		} 
		echo "</br> Reverse Number";
		echo " ".$this->reverse_number(5674);
		
		echo "</br> Reverse String --";
		echo $this->reverse_tring('RAJAN');
	}   
	public function PalindromeNumber($n){  
		$number = $n;  
		$sum = 0;  
		while(floor($number)) {  
		$rem = $number % 10;  
		$sum = $sum * 10 + $rem;  
		$number = $number/10;  
		}  
		return $sum;  
		} 
		
	function reverse_number($number){
	 
		$sum = 0;
	 
		while( floor($number) ) {
	 
			$newnum = $number % 10;
			$sum = $sum * 10 + $newnum;
	  
			$number = $number/10;
		}
	 
	   return $sum;
	}
	function reverse_tring($str){
		for($i=strlen($str)-1, $j=0; $j<$i; $i--, $j++)  
		{ 
			$temp = $str[$i]; 
			$str[$i] = $str[$j]; 
			$str[$j] = $temp; 
		} 
		return $str; 
		
	}
}
