-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2020 at 06:40 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpapidb`
--

-- --------------------------------------------------------

--
-- Table structure for table `weather_info`
--

CREATE TABLE `weather_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `temp_max` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `temp_min` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `humidity` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `speed` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `weather_info`
--

INSERT INTO `weather_info` (`id`, `name`, `description`, `temp_max`, `temp_min`, `humidity`, `speed`) VALUES
(11, 'London', 'broken clouds', '18.89', '16.67', '77', '2.6'),
(12, 'London', 'broken clouds', '18.89', '16.67', '77', '2.6'),
(13, 'London', 'Scattered Clouds', '20', '17.22', '68', '2.6'),
(14, 'London', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(15, 'London333', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(16, 'London333', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(17, 'London555', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(18, 'London555', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(19, 'London', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(20, 'London', 'Broken Clouds', '20', '17.22', '59', '3.6'),
(21, 'London', 'Broken Clouds', '20', '17.22', '59', '3.6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weather_info`
--
ALTER TABLE `weather_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `weather_info`
--
ALTER TABLE `weather_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
